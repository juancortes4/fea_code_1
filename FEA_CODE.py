import math
import numpy as np


node_coords = [0.0, 0.0, 1500.0, 0.0, 3000.0, 0.0, 2250.0, 750.0, 750.0, 750.0]
elem_node = [1, 2, 2, 3, 5, 4, 1, 5, 2, 5, 2, 4, 3, 4]
n1=[0.0, 0.0]
n2=[1500.0, 0.0]
n3=[3000.0, 0.0]
n4=[2250.0, 750]
n5=[750.0, 750.0]
elem_node2 = [n1, n2, n2, n3, n5, n4, n1, n5, n2, n5, n2, n4, n3, n4]
b=len(elem_node)/2
E_el = 70000*np.ones(len(elem_node))
A_el = 100*np.ones(len(elem_node))
nod_f = [2]
f_dir = [2]
f_mag = [-10000]
nod_bc = [1, 1, 3]
bc_dir = [1, 2, 2]
bc_mag = [0, 0, 0]
plot_flag = True
screen_output_flag = True
disp_scale_factor = 0.5
force_scale_factor = 0.2

#def truss2d(node_coords,elem_node,b,E_el,A_el,nod_f,f_dir,f_mag,nod_bc,bc_dir,bc_mag,plot_flag,screen_output_flag,disp_scale_factor,force_scale_factor):


def pre_procces(node_coords,elem_node,nod_f,f_dir,nod_bc,bc_dir):
    n_nod=(len(node_coords))/2
    n_el=(len(elem_node))/2
    n_el=int(n_el)
    n_f=len(nod_f)
    n_bc=len(nod_bc)
    f_dof=np.ones(n_f)
    bc_doff=np.ones(n_bc)
    for ifor in range(0,n_f):
        if f_dir[ifor] == 1:
            force_dof = 2*nod_f[ifor]-1
        elif f_dir[ifor] == 2:
            force_dof = 2*nod_f[ifor]
        f_dof[ifor] = force_dof
    for ibc in range(0, n_bc):
        if bc_dir[ibc] == 1:
            bc_dof = 2*nod_bc[ibc] - 1
        elif bc_dir[ibc] == 2:
            bc_dof = 2*nod_bc[ibc]
        bc_doff[ibc] = bc_dof;
    el_phi = np.zeros(n_el)
    el_length = np.zeros(n_el)
    h=len(elem_node2)
    for iel in range(0,h,2):
        Gnode1 = elem_node2[iel]
        Gnode2 = elem_node2[iel+1]
        Cordy = Gnode2[1]-Gnode1[1]
        Cordx = Gnode2[0]-Gnode1[0]
        delta = ((Cordy**2)+(Cordx**2))**0.5
        h1=int(iel/2)
        el_length[h1] = delta
        angle = math.atan2(Cordy,Cordx)
        el_phi[h1] = angle
    return el_phi,el_length,bc_doff,f_dof,n_nod,n_el

el_phi,el_length,bc_doff,f_dof,n_nod,n_el =  pre_procces(node_coords,elem_node,nod_f,f_dir,nod_bc,bc_dir)

def compute_element_stiffness(iel,E_el,A_el,el_length,el_phi):
    iel=int(iel/2)
    ke = A_el[iel]*E_el[iel]/el_length[iel]
    phi = el_phi[iel]
    c = math.cos(phi)
    s = math.sin(phi)
    Kel =[ c**2,c*s,-c**2,-c*s,
             c*s,s**2,-c*s,-s**2,
             -c**2,-c*s,c**2,c*s,
             -c*s,-s**2,c*s,s**2]
    d=np.zeros((4,4))
    h=0
    for i in range(0,4):
        for j in range(0,4):
            d[i,j]=ke*Kel[h]
            h=h+1
    return d

def assemble_system(n_nod,n_el,elem_node,f_dof,bc_doff):
    a=int(2*n_nod)
    K = np.zeros((a,a))
    u = np.zeros((a,1))
    f = np.zeros((a,1))
    n_el=int(n_el*2)
    for iel in range(0,n_el,2):
        Kel=compute_element_stiffness(iel,E_el,A_el,el_length,el_phi)
        Gnode1 = elem_node[iel]
        Gnode2 = elem_node[iel+1]
        dofs = [2*(Gnode1-1) , 2*Gnode1-1, 2*(Gnode2-1) , 2*Gnode2-1]
        for i in range(0,4):
            for j in range(0,4):
                K[dofs[i],dofs[j]] = K[dofs[i], dofs[j]] + Kel[i,j]
    for g in range(0,len(f_dof)):
        f[int(f_dof[g]-1)] = f_mag[g]
    for h in range(0,len(bc_doff)):
        u[int(bc_doff[h]-1)] = bc_mag[h]
    return K,u,f
K,u,f = assemble_system(n_nod,n_el,elem_node,f_dof,bc_doff)


def solve_system(K,u,f,n_nod,bc_doff):
    for i2 in range(0,len(bc_doff)):
        bc_doff[i2]=bc_doff[i2]-1
    alldofs = np.ones(int(2*n_nod))
    j=int(len(alldofs))
    for i in range(0,j):
        alldofs[i]=(i*alldofs[i])
    fixeddofs=bc_doff
    freedofs = np.setdiff1d(alldofs,fixeddofs)
    j2=int(len(bc_doff))
    j5=int(len(freedofs))
    j6=int(len(alldofs))
    KE= np.zeros((j2,j2))
    KEF= np.zeros((j2,j5))
    dE=np.zeros((j2,1))
    FF=np.zeros((j5,1))
    KF=np.zeros((j5,j5))
    for i3 in range(0,len(fixeddofs)):
        for i4 in range(0,len(fixeddofs)):
            j3=int(fixeddofs[i3])
            j4=int(fixeddofs[i4])
            KE[i3,i4]=K[j3,j4]
    for i5 in range(0,j2):
        for i6 in range(0,j5):
            j7=int(fixeddofs[i5])
            j8=int(freedofs[i6])
            KEF[i5,i6]=K[j7,j8]
    for i7 in range(0,j2):
        j9=int(fixeddofs[i7])
        dE[i7]=u[j9]
    for i8 in range(0,j5):
        j10=int(freedofs[i8])
        FF[i8]=f[j10]
    for i9 in range(0,j5):
        for i10 in range(0,j5):
            j11=int(freedofs[i9])
            j12=int(freedofs[i10])
            KF[i9,i10]=K[j11,j12]
    KEFT=KEF.transpose()
    k=np.dot(KEFT,dE)
    dFf =(FF - k)
    dFe=np.linalg.lstsq(KF,dFf)
    dF=dFe[0]
    fe=np.dot(KE,dE)
    fee=np.dot(KEF,dF)
    FE = fe+fee
    for r in range(0,j5):
        r1=int(freedofs[r])
        u[r1]=dF[r]
    for r2 in range(0,j2):
        r3=int(fixeddofs[r2])
        f[r3]=FE[r2]
    return u,f
u,f = solve_system(K,u,f,n_nod,bc_doff)

def post_process(n_el,elem_node,u,f,el_phi,screen_output_flag,n_nod):
    bar_force = np.zeros((n_el, 1))
    bar_stress = np.zeros((n_el, 1))
    n_el=int(n_el*2)
    for iel in range(0,n_el,2):
        K_el=compute_element_stiffness(iel,E_el,A_el,el_length,el_phi)
        Gnode1 = elem_node[iel]
        Gnode2 = elem_node[iel+1]
        dofs = [2*Gnode1-2,2*Gnode1-1,2*Gnode2-2,2*Gnode2-1]
        d=len(dofs)
        u_el=np.zeros((d,1))
        for i in range(0,d):
            j=int(dofs[i])
            u_el[i]=u[j]
        f_el=np.dot(K_el,u_el)
        phi=el_phi[int(iel/2)]
        bar_force[int(iel/2)] = -(np.cos(phi)*f_el[1] + np.sin(phi)*f_el[2])
        bar_stress[int(iel/2)] = bar_force[int(iel/2)]/A_el[iel]
    if screen_output_flag==True:
        print('==============')
        print('DISPLACEMENTS ')
        print('==============')
        print('Node, u_x, u_y     \n')
        print('-------------------\n')
        for inod in range(0,int(n_nod)):
            print(inod, u[int(2*inod)], u[int(2*inod+1)])

post_process(n_el,elem_node,u,f,el_phi,screen_output_flag,n_nod)
